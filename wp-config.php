<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true);
define( 'WPCACHEHOME', '/var/www/html/programmingbud/wp-content/plugins/wp-super-cache/' );
define('DB_NAME', 'programming_bud');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'LuvUrL!f3');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
define('FS_METHOD', 'direct');
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'GNE,ByeMx#(3UI=$zQ<P;Y?UdlW~2##H:MMf_b iOe$AFmf^Y(Q8_ONK/u<jQUvV');
define('SECURE_AUTH_KEY',  'wW]v_>.G$Nva@(LfYM,qPI-!u%cC{E#{DD){8?C#zA>:e|Iv--?.-3BlJ]xJ1tvg');
define('LOGGED_IN_KEY',    ';X`#cP)3o<=lL,NAzmH 6]Zg]FfF9 #w @MnOm2EJG;#bnOWZ$*`|pGpT$hZ{=.Q');
define('NONCE_KEY',        '[%,27k!8LH<7t+{Z_3Q/M~bD>>e905]HsQVvpodW@cohF]_iz~Bw.vwcK-_Qb2<C');
define('AUTH_SALT',        'uyMPv^T.UD#=LS. {,Vu ]v>5z9(jiXj$>.n{_}pxR:U[@SAa*k+Q0^Yqi]*sa8?');
define('SECURE_AUTH_SALT', '0&mo3MIp51T57zN,N2TD:ZM24=f<to(skKV(hC=!+E?o`%nbbD-o! Iz)1rgR@[n');
define('LOGGED_IN_SALT',   'b`?h}!YI[WWjT)YMy>w/`d8;q]FMZ}! )8:()Gmn?gA#@o:s=OtpmVgb}l>k@&Ri');
define('NONCE_SALT',       '#2wVoZ}>G|LGUVBUGr]xKZ*9=ujxd58qh`Z1<tv( @/AhK@(`=tR)5@:eo<.b3nb');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'pw_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('WP_HOME', 'https://programmingbud.thadaninilesh.com');
define('WP_SITEURL', 'https://programmingbud.thadaninilesh.com');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
